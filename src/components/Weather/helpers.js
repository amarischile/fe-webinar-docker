
const getWeather = async (setWeather) => {
    fetch(process.env.REACT_APP_WEATHER_API)
    .then(response => response.json())
    .then(data => setWeather(data))
    .catch(e => console.log(`Error on request ${process.env.REACT_APP_WEATHER_API}`))
}

export default {
    getWeather
}