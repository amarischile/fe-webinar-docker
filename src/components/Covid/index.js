import React, { useState, useEffect } from 'react';
import helpers from './helpers'
import Page from './page'

function Covid() {
    const [covid, setCovid] = useState({});

    useEffect(() => {
        helpers.getCovid(setCovid)
    },[setCovid]);

    return (
        <Page covid={covid} />
    );
}
  
export default Covid;