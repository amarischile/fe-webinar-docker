import React, { useState, useEffect } from 'react';
import helpers from './helpers'
import Page from './page'

function ExchangeRate() {
    const [exchangeRate, setExchangeRate] = useState({});

    useEffect(() => {
        helpers.getExchangeRate(setExchangeRate)
    },[setExchangeRate]);

    return (
        <Page exchangeRate={exchangeRate} />
    );
}
  
export default ExchangeRate;