
const getExchangeRate = async (setExchangeRate) => {
    fetch(process.env.REACT_APP_EXCHANGE_RATE_API)
    .then(response => response.json())
    .then(data => setExchangeRate(data))
    .catch(e => console.log(`Error on request ${process.env.REACT_APP_EXCHANGE_RATE_API}`))
}

export default {
    getExchangeRate
}