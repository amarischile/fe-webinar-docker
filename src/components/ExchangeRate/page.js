import React, { Fragment } from 'react';

import './style.css'

function Page(props) {
    console.log(props)
    const { exchangeRate } = props
    return (
        <div className="col-md card-common exchange">
            {undefined !== exchangeRate.countryCode && <Fragment>
            <div className="row">
                <div className="col-12">
                    <h3 className="title">Exchange Rate</h3>
                    <h3 className="subtitle">Country {exchangeRate.countryCode.toUpperCase()}</h3>
                </div>
            </div>
            <div className="row middle">
                <div className="col-4 middle-row">
                    <span className="currency">USD</span>
                    <h1 className="value">1</h1>
                </div>
                <div className="icon">
                    <span className="material-icons">arrow_right_alt</span>
                </div>
                <div className="col-8 middle-row text-right">
                    <span className="currency">{exchangeRate.to.toUpperCase()}</span>
                    <h1 className="value">{exchangeRate.value.toFixed(1).replace('.', ',')}</h1>
                </div>
            </div>
            </Fragment>}
        </div>
    );
}

export default Page;