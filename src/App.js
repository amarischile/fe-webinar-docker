import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import React from 'react';
import Weather from './components/Weather'
import ExchangeRate from './components/ExchangeRate'
import Covid from './components/Covid'
import { createBrowserHistory } from 'history';


export const history = createBrowserHistory({
    basename: process.env.PUBLIC_URL
});

function App() {
    return (
        <div className="container wrapper">
            <div className="row">
                <div className="col-8 offset-md-2">
                    <Covid />
                </div>
            </div>
            <div className="row">
                <div className="col-4 offset-md-2">
                    <Weather />
                </div>
                <div className="col-4">
                    <ExchangeRate />
                </div>
            </div>
        </div>
    );
}

export default App;
