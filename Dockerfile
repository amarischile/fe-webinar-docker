FROM node:12-slim as build

ARG REACT_APP_WEATHER_API=http://localhost/webinar-docker/api/weather/
ARG REACT_APP_EXCHANGE_RATE_API=http://localhost/webinar-docker/api/exchange-rate/
ARG REACT_APP_COVID_API=http://localhost/webinar-docker/api/covid-19/
ARG PUBLIC_URL=/webinar-docker

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH
COPY package*.json ./
RUN npm ci --silent
RUN npm install react-scripts@3.4.1 -g --silent

COPY . ./
RUN npm run build

FROM nginx:alpine

WORKDIR /usr/share/nginx/html/webinar-docker

COPY --from=build /app/nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=build /app/build /usr/share/nginx/html/webinar-docker

EXPOSE 90
CMD ["nginx", "-g", "daemon off;"]