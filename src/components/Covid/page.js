import React, {Fragment} from 'react';

import './style.css'

function Page(props) {
    const { covid } = props
    return (
        <div className="col-md card-common covid">
            {undefined !== covid.data && <Fragment>
                <div className="row">
                    <div className="col-12">
                        <h3 className="title">Covid-19</h3>
                        <h3 className="subtitle">Country {covid.data.country} ({covid.data.iso.toUpperCase()})</h3>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md middle-row text-left">
                        <span className="total">Confirmed</span>
                        <h1 className="value">{covid.data.confirmed}</h1>
                    </div>
                    <div className="col-md middle-row text-center">
                        <span className="total">Recovered</span>
                        <h1 className="value">{covid.data.recovered}</h1>
                    </div>
                    <div className="col-md middle-row text-right">
                        <span className="total">Deaths</span>
                        <h1 className="value">{covid.data.deaths}</h1>
                    </div>
                </div>
            </Fragment>}
        </div>
    );
}
  
export default Page;