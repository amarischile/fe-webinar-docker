import React, { useState, useEffect } from 'react';
import helpers from './helpers'
import Page from './page'

function Weather() {
    const [weather, setWeather] = useState({});

    useEffect(() => {
        helpers.getWeather(setWeather)
    },[setWeather]);

    return (
        <Page weather={weather} />
    );
}
  
export default Weather;