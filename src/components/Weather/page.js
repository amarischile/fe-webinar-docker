import React, {Fragment} from 'react';

import './style.css'

function Page(props) {
    const { weather } = props
    return (
        <div className="col-md card-common weather">
            {undefined !== weather.country && <Fragment>
                <div className="row">
                    <div className="col-12">
                        <h3 className="title">{weather.city}, {weather.country.code.toUpperCase()}</h3>
                        <h4 className="subtitle">{weather.date.formated}, {weather.weather.description}</h4>
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 middle-row">
                        <h1 className="temperature">{weather.temperature.celcius}°C</h1>
                    </div>
                    <div className="col-6 middle-row text-right">
                        <img className="icon" src={weather.weather.icon} alt="Temperature" />
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 last-row">
                        <p className="wind extra-icon">{weather.wind.speed} km/h Wind</p>
                        <p className="humidity extra-icon">{weather.humidity}% Humidity</p>
                    </div>
                </div>
            </Fragment>}
        </div>
    );
}
  
export default Page;