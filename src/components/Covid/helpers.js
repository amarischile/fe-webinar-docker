
const getCovid = async (setCovid) => {
    fetch(process.env.REACT_APP_COVID_API)
    .then(response => response.json())
    .then(data => setCovid(data))
    .catch(e => console.log(`Error on request ${process.env.REACT_APP_COVID_API}`))
}

export default {
    getCovid
}